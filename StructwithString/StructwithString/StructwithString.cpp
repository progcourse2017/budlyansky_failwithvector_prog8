// StructwithString.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct IandS
{
	int integerS;
	string lineS;
};

void enterTheData(IandS & yourStruct);

int main()
{
	vector <IandS> memory;
	IandS insertInMem;

	bool keyW = false;
	int keyS;

	while(!keyW)
	{
		system("cls");
		cout << "Enter the key of operations:\n"
			<< "1 - New data\n"
			<< "2 - Get size of vector\n"
			<< "3 - Clear memory\n"
			<< "4 - Exit \n"
			<< "Your choise: ";

		cin >> keyS;

		switch(keyS)
		{
		case 1:
			enterTheData(insertInMem);
			memory.push_back(insertInMem);
			break;
		case 2:
			cout << memory.size() << endl;
			system("pause");
			break;

		case 3:
			memory.clear();
			memset(&insertInMem, 0, sizeof(IandS));
			break;


		case 4:
			memory.clear();
			memset(&insertInMem, 0, sizeof(IandS));
			exit(1);
			

		default: 
			cout << "Incorrect key\n\n\n";
			system("pause");
			break;
		}

	};

    return 0;
}

void enterTheData(IandS & yourStruct) 
{

	cout << "Any integer value: ";
	cin >> yourStruct.integerS;

	cout << "Any string: ";
	cin >> yourStruct.lineS;

};